Django>=3.0,<4.0
psycopg2>=2.8
djangorestframework==3.13.1
django-contact-form
requests[security] == 2.27.1
urllib3 == 1.26.8
Pillow
fontawesomefree
django-bootstrap-v5
requests_toolbelt 
Werkzeug
firebase-admin

"""cms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from module.ProductModule.controller import ProductDashboardController,ProductPostController,ProductUpdateController,ProductDashboardMultipleDelete
from module.UserModule.controller import UserDashboardController,UserPostController,UserUpdateController
from module.StatisticalModule.controller import StatisticalDashboardController
from module.AdminLoginModule.controller import AdminController
from module.LogoutModule.controller import LogoutController
from django.conf import settings
from django.conf.urls.static import static
app_name = "module"
urlpatterns = [
    path('dashboard/', ProductDashboardController.Dashboard.index, name ='dashboard'),
    path('dashboard_sort_az/', ProductDashboardController.Dashboard.sort_za, name ='sort_za'),

    path('dashboard/<int:id>/',ProductDashboardController.Dashboard.delete_product, name ='delete'),
    path('getformproduct/',ProductPostController.GetFormProduct, name ='getformproduct'),
    path('post_product/',ProductPostController.PostProduct, name ='post_product'),
    path('getupdateproduct/<int:id>',ProductUpdateController.index, name ='getupdateproduct'),
    path('updateproduct/<int:id>',ProductUpdateController.update, name ='updateproduct'),



    path('dashboarduser/', UserDashboardController.index, name ='dashboarduser'),
    path('dashboarduser_sort_a_z/', UserDashboardController.sort_za_user, name ='sort_za_user'),
    path('dashboarduser/<int:id>',UserDashboardController.delete_user, name ='deleteuser'),
    path('getformuser/',UserPostController.GetFormUser, name ='getformuser'),
    path('post_user/',UserPostController.PostUser, name ='post_user'),
    path('getupdateuser/<int:id>',UserUpdateController.index, name ='getupdateuser'),
    path('update_user/<int:id>',UserUpdateController.update, name ='update_user'),


    path('getstatistical/',StatisticalDashboardController.Dashboard.index, name ='getstatistical'),
    path('adminLogin/', AdminController.index, name = 'adminLogin'),
    path('logout/', AdminController.logout, name = 'logout'),





    

    path('admin/', admin.site.urls),
]+ static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

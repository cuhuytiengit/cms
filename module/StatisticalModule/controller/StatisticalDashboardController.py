from atexit import register
from contextlib import redirect_stderr
from django import template
from django.template import loader
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect
import requests,urllib3
import collections 
import json
# from ..module.ProductModule import view
import sys
if sys.version_info.major == 3 and sys.version_info.minor >= 10:
    from collections.abc import MutableMapping
else:
    from collections import MutableMapping
register=template.Library()

class Dashboard():
    
    def index(request):
        if request.method == 'GET':
            login_get = json.loads(json.dumps(request.session.get('login')))
            print('login_get')
            if(request.session.get('login')):
                admin_get = requests.get('http://192.168.1.25:5000/get-admin-by-id/'+str(login_get)).json()
            else:
                return redirect('/dashboard/adminLogin/')


            print(login_get)
            statistical_list = requests.get('http://192.168.1.25:5000/get_statistical_user_visit/').json()
            statistical_list_get = json.loads(json.dumps(statistical_list))
            print(statistical_list_get)
            print(type(statistical_list_get))
            list_date_access = []
            list_week_access = []
            list_month_access = []
            for user_access in statistical_list_get :
          
                print(user_access['date_access'])
                date_acess =user_access['date_access']
                access_id =user_access['id']
                access_week =user_access['week_access']
                access_month =user_access['month_access']
                statistical_date = requests.get('http://192.168.1.25:5000/get_statistical_user_visit_id/'+str(access_id)).json()
                print(statistical_date)
                
                list_date_access.append(date_acess)
                count_access_date = list_date_access.count(date_acess)

                list_week_access.append(access_week)
                list_month_access.append(access_month)

                print(count_access_date)

                
            print(list_date_access)
            freq = {}
            for items in list_date_access:
                freq[items] = list_date_access.count(items)
            for key, value in freq.items():
                print ("% s : % d"%(key, value))

            print(list_week_access)
            week = {}
            for items in list_week_access:
                week[items] = list_week_access.count(items)
            for key, value in week.items():
                print ("% s : % d"%(key, value))
            

            print(list_month_access)
            month = {}
            for items in list_month_access:
                month[items] = list_month_access.count(items)
            for key, value in month.items():
                print ("% s : % d"%(key, value))
                

            view_website = requests.get('http://192.168.1.25:5000/get_statistical_visit/').json()
            print(view_website)
            print(type(view_website))
            statistical_list_view_website_get = json.loads(json.dumps(view_website))
            print(statistical_list_view_website_get)
            list_ip_access = []
            for user_access_ip in statistical_list_view_website_get:
                access_ip_id =user_access_ip['id']
                list_ip_access.append(access_ip_id )
                count_access_date = list_ip_access.count(access_ip_id)
                print(count_access_date)

         
            ip = {}
            for items in list_ip_access:
                ip[items] = list_ip_access.count(items)
            for key, value in ip.items():
                print ("% s : % d"%(key, value))
            sumcountaccessdate = sum(list(ip.values()))


            statistical_checkout_list = requests.get('http://192.168.1.25:5000/checkout/').json()
            statistical_list_checkout_get = json.loads(json.dumps(statistical_checkout_list))
            print(statistical_list_checkout_get)

            list_checkout=[]

            for get_id in statistical_list_checkout_get:
                get_id_checkout = get_id['id']
                list_checkout.append(get_id_checkout)
            
            id_checkout = {}
            for items in list_checkout:
                id_checkout[items] = list_checkout.count(items)
            for key, value in id_checkout.items():
                print ("% s : % d"%(key, value))
            sumcheckout = sum(list(id_checkout.values()))

            sum_price_checkout =0
            for get_price_checkout in statistical_list_checkout_get:
                price_checkout = get_price_checkout['price']
                print(price_checkout)
                sum_price_checkout += price_checkout
            sum_price_checkout_ex = "{:,}".format(sum_price_checkout)




            for get_checkout in statistical_list_checkout_get :
                get_product =get_checkout['product']
                get_id_user =get_checkout['user_id']
                
                print( get_product )

                statistical_user_list = requests.get('http://192.168.1.25:5000/get-user-by-id/'+str(get_id_user)).json()
                statistical_list_checkout_product_get = json.loads(json.dumps(get_product))
                print('statistical_list_checkout_product_get' + " " + str(get_product) )
               
                id_append=[]
                
                for get_statistical_list_checkout_product_get in statistical_list_checkout_product_get:
                    get_id_product =get_statistical_list_checkout_product_get['id']
                    print(get_id_product)

                    statistical_product_list = requests.get('http://192.168.1.25:5000/get-product-by-id/'+str(get_id_product)).json()
                    print('statistical_product_list')
                    print(type(statistical_product_list))
                    print(statistical_product_list)
                    id_append.append(get_id_product)
                


            return render(request,'StatisticalDashboardView.html',{'day_access': freq.keys(), 'count_access': freq.values(),"week_access": week.keys(), "count_week_access": week.values(),"month_access": month.keys(), "count_month_access": month.values(),'sumcountaccessdate':sumcountaccessdate,'listcheckout': statistical_checkout_list,'sumcheckout':sumcheckout,'sumpricecheckout':sum_price_checkout_ex,'admin_get':admin_get})
    
        
from unicodedata import name
from django.core import validators
from django import forms 
from django.db import models  
from django.forms import fields  
from module.ProductModule.Model.ProductModel import ProductModel

class Product(forms.Form):
   image = forms.ImageField(label='Image',required = True)
   productname = forms.CharField(label='Name product',required = False)
   price = forms.IntegerField(label='Price',required = False)
   description =  forms.CharField(label='Description',widget=forms.Textarea,required = False)
   class Meta:
      model =  ProductModel
      fields = ('image', 'productname', 'price', 'description')


class Product_Edit(forms.Form):
   image = forms.ImageField(label='Image')
   productname = forms.CharField(label='Name product')
   price = forms.IntegerField(label='Price')
   description =  forms.CharField(label='Description',widget=forms.Textarea)
   class Meta:
    model =  ProductModel
    fields = ('image','productname', 'price', 'description')
     
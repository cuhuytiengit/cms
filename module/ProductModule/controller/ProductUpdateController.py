from contextlib import redirect_stderr
from itertools import product
import json
from multiprocessing import context
from django.template import loader
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect
import requests,urllib3
import collections 
import os
from PIL import Image

from module.ProductModule.form.forms_product import Product_Edit
import sys
if sys.version_info.major == 3 and sys.version_info.minor >= 10:
    from collections.abc import MutableMapping
else:
    from collections import MutableMapping


def index(request,id):
    if request.method == 'GET':
        login_get = json.loads(json.dumps(request.session.get('login')))
        print('login_get')
        if(request.session.get('login')):
            admin_get = requests.get('http://192.168.1.25:5000/get-admin-by-id/'+str(login_get)).json()
        else:
            return redirect('/dashboard/adminLogin/')
        data = requests.get(f'http://192.168.1.25:5000/get-product-by-id/{id}',headers = {'Content-Type' : 'application/json'}).json()
        print(data)
        form=Product_Edit(initial=data)
        mydict = {'form' : form,'dataid' : data,'admin_get':admin_get }
        return render(request,'ProductUpdateView.html',context = mydict)
    # else:
    #     return redirect('/dashboard/dashboard/')

def update(request,id):
    context = {}
    ROOT_FOLDER = os.path.dirname(os.path.abspath(__file__))
    print(ROOT_FOLDER)
    target = os.path.join(ROOT_FOLDER,'images')
    print(target)
    if not os.path.isdir(target):
        os.mkdir(target)
  
    if request.method == 'POST':
        form = Product_Edit(request.POST, request.FILES)
        if form.is_valid():
            image = request.FILES['image']
            image_name = image.name 
            img = Image.open(image)
            abs_file_path = '/'.join([target,image.name])
            img.save(abs_file_path)
            img.close()
            print('ok nha mn')
            productname = form.cleaned_data['productname']
            price = form.cleaned_data['price']
            description = form.cleaned_data['description']
            abs_file_path_form = '/'.join([target, image_name])
            print(abs_file_path_form)
            multipart_form_data = {
                'image': (image_name, open(abs_file_path_form, 'rb') ),
                'productname': (None, productname),
                'price': (None, price),
                'description': (None, description)
            }
            data = requests.post('http://192.168.1.25:5000/update_product/'+ str(id),files=multipart_form_data)
            print(data.text)
        else:
            form = Product_Edit() 
        
        return redirect('/dashboard/dashboard/')
        
        
   

   










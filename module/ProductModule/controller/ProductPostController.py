from pydoc import resolve
from tkinter.filedialog import Open
from requests_toolbelt import MultipartEncoder
from django.shortcuts import redirect
from urllib import response
from django.template import loader
from django.shortcuts import render
from django.http import HttpResponse
import requests,urllib3
import collections 
from werkzeug.utils import secure_filename
from werkzeug.datastructures import  FileStorage
from io import BytesIO
from fileinput import filename
from django.http import FileResponse
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.utils.datastructures import MultiValueDictKeyError
import sys
import base64
from django.conf import settings
import os
import io 
from django.core.files.storage import FileSystemStorage
from django.core.files import File
from requests.exceptions import ConnectionError
from PIL import Image
# from somewhere import handle_uploaded_file
import requests
import json
from django.shortcuts import redirect
from module.ProductModule.form.forms_product import Product

ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


app_root = os.path.dirname(os.path.abspath(__file__))
if sys.version_info.major == 3 and sys.version_info.minor >= 10:
    from collections.abc import MutableMapping
else:
    from collections import MutableMapping




def GetFormProduct(request):
    login_get = json.loads(json.dumps(request.session.get('login')))
    print('login_get')
    if(request.session.get('login')):
        admin_get = requests.get('http://192.168.1.25:5000/get-admin-by-id/'+str(login_get)).json()
    else:
        return redirect('/dashboard/adminLogin/')
    form = Product(request.GET, request.FILES)
    return render(request,'ProductPostView.html',{'form':form,'admin_get':admin_get})


def PostProduct(request):
    context = {}
    ROOT_FOLDER = os.path.dirname(os.path.abspath(__file__))
    print(ROOT_FOLDER)
    target = os.path.join(ROOT_FOLDER,'images')
    print(target)
    if not os.path.isdir(target):
        os.mkdir(target)

    if request.method == 'POST':
        form = Product(request.POST, request.FILES)
        if form.is_valid():
            image = request.FILES['image']
            image_name = image.name
            img = Image.open(image)
            abs_file_path = '/'.join([target,image.name])
            img.save(abs_file_path)
            img.close()
            productname = form.cleaned_data['productname']
            price = form.cleaned_data['price']
            description = form.cleaned_data['description']
            print(image_name)
            abs_file_path_form = '/'.join([target, image_name])
            print(abs_file_path_form)
            multipart_form_data = {
                'image': (image_name, open(abs_file_path_form, 'rb')),
                'productname': (None, productname),
                'price': (None, price),
                'description': (None, description)
            }
            print(type(multipart_form_data))
            r = requests.post('http://192.168.1.25:5000/post-product/',files=multipart_form_data)
            print(r.text)
        else:
            form = Product() 
            print('post fail')
            return redirect('/dashboard/getformproduct/')

        return redirect('/dashboard/dashboard/')




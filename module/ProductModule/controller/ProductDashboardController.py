from atexit import register
from contextlib import redirect_stderr
from django import template
from django.template import loader
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect
import requests,urllib3
import collections 
import os, base64
import json
from django.core.paginator import Paginator
# from ..module.ProductModule import view
import sys
if sys.version_info.major == 3 and sys.version_info.minor >= 10:
    from collections.abc import MutableMapping
else:
    from collections import MutableMapping
register=template.Library()
payload = {'uid' : 'cuhuytien',
      'pwd' : 'cuhuytien'}
class Dashboard():
    
    def index(request):
        if request.method == 'POST':
            product_delete = request.POST.getlist('product_delete')
            print(product_delete)
            print(type(product_delete))
            for product_del in product_delete:
                product = requests.get('http://192.168.1.25:5000/delete_product/'+str(product_del))
                print(product)

            return redirect('/dashboard/dashboard/')

        if request.method == 'GET':
        
            login_get = json.loads(json.dumps(request.session.get('login')))
            print('login_get')
            product_list = requests.get('http://192.168.1.25:5000/sort-product-a-z/',data=payload).json()
            if(request.session.get('login')):
                admin_get = requests.get('http://192.168.1.25:5000/get-admin-by-id/'+str(login_get)).json()
            else:
                return redirect('/dashboard/adminLogin/')


            print(login_get)
            product_list = requests.get('http://192.168.1.25:5000/sort-product-a-z/',data=payload).json()
            p = Paginator(product_list,3)
            page=request.GET.get('page')
            number_page = p.get_page(page)
            context = {'product_data' : product_list, 'number_page':number_page, 'admin_get':admin_get, 'number_item':p}
            return render(request,'ProductDashboardView.html',context)
        
    def sort_az(request):
        if request.method == 'GET':
        
            login_get = json.loads(json.dumps(request.session.get('login')))
            print('login_get')
            product_list = requests.get('http://192.168.1.25:5000/sort-product-a-z/',data=payload).json()
            if(request.session.get('login')):
                admin_get = requests.get('http://192.168.1.25:5000/get-admin-by-id/'+str(login_get)).json()
            else:
                return redirect('/dashboard/adminLogin/')


            print(login_get)
            product_list = requests.get('http://192.168.1.25:5000/sort-product-a-z/',data=payload).json()
            p = Paginator(product_list,3)
            page=request.GET.get('page')
            number_page = p.get_page(page)
            context = {'product_data' : product_list, 'number_page':number_page, 'admin_get':admin_get, 'number_item':p}
            return render(request,'ProductDashboardView.html',context)

    def sort_za(request):
        if request.method == 'GET':
            login_get = json.loads(json.dumps(request.session.get('login')))
            print('login_get')
            product_list = requests.get('http://192.168.1.25:5000/sort-product-z-a/',data=payload).json()
            if(request.session.get('login')):
                admin_get = requests.get('http://192.168.1.25:5000/get-admin-by-id/'+str(login_get)).json()
            else:
                return redirect('/dashboard/adminLogin/')


            print(login_get)
            product_list = requests.get('http://192.168.1.25:5000/sort-product-z-a/',data=payload).json()
            p = Paginator(product_list,3)
            page=request.GET.get('page')
            number_page = p.get_page(page)
            context = {'product_data' : product_list, 'number_page':number_page, 'admin_get':admin_get, 'number_item':p}
            return render(request,'ProductDashboardViewSortZA.html',context)

    
    def delete_product(request,id):
        print('test delete')
        if request.method == 'GET':
            product_list = requests.get('http://192.168.1.25:5000/delete_product/'+ str(id))
            context = {'product_data' : product_list}
            return redirect('/dashboard/dashboard/')

    

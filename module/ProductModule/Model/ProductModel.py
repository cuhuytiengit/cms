from os import name
from unittest.util import _MAX_LENGTH
from django.db import models

# Create your models here.


class ProductModel(models.Model):
    productname = models.CharField(max_length=250)
    price = models.IntegerField()
    description =  models.CharField(max_length=300)
    image = models.ImageField(upload_to='images/',name='image')
    def __str__(self):
        return self.image,self.productname, self.price, self.description

from unicodedata import name
from django.core import validators
from django import forms 
from django.db import models  
from django.forms import fields  
from ..Model.UserModel import  UserModel

class User(forms.Form):
   image = forms.ImageField(label='Image')
   firstname = forms.CharField(label='Firstname')
   lastname = forms.CharField(label='Lastname')
   password =  forms.CharField(label='Password')
   email = forms.CharField(label='Email')
   address =  forms.CharField(label='Address')
   phone =  forms.CharField(label='phone')

   
   class Meta:
    model =  UserModel
    fields = ('image', 'firstname', 'lastname', 'password','email','address','phone')


class User_Edit(forms.Form):
   image = forms.ImageField(label='Image')
   firstname = forms.CharField(label='Firstname')
   lastname = forms.CharField(label='Lastname')
   password =  forms.CharField(label='Password')
   email = forms.CharField(label='Email')
   address =  forms.CharField(label='Address')
   phone =  forms.CharField(label='phone')
   class Meta:
    model =  UserModel
    fields = ('firstname', 'lastname', 'password','email','address','phone')
     
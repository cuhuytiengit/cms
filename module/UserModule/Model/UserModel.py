from os import name
from unittest.util import _MAX_LENGTH
from django.db import models

# Create your models here.


class UserModel(models.Model):
    firstname = models.CharField(max_length=250)
    lastname = models.CharField(max_length=300)
    password =  models.CharField(max_length=300)
    email =  models.CharField(max_length=300)
    address =  models.CharField(max_length=300)
    phone =  models.CharField(max_length=10)
    image = models.ImageField(upload_to='image/',max_length=200,name='image')
    def __str__(self):
        return self.firstname, self.lastname, self.password,self.email,self.address,self.phone,self.image

from pydoc import resolve
from django.shortcuts import redirect
from urllib import response
from django.template import loader
from django.shortcuts import render
from django.http import HttpResponse
import requests,urllib3
import collections 
from fileinput import filename
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.utils.datastructures import MultiValueDictKeyError
import sys
import base64
import os
import io 
from requests.exceptions import ConnectionError
from PIL import Image
import requests
import json
from django.shortcuts import redirect
from module.UserModule.form.forms_user import User

app_root = os.path.dirname(os.path.abspath(__file__))
if sys.version_info.major == 3 and sys.version_info.minor >= 10:
    from collections.abc import MutableMapping
else:
    from collections import MutableMapping

def filename(data):
    try:
        data = base64.b64decode(data.encode('UTF-8'))
        buf = io.BytesIO(data)
        img = Image.open(buf)
        return img

    except:
        return None



def GetFormUser(request):
    login_get = json.loads(json.dumps(request.session.get('login')))
    print('login_get')
    if(request.session.get('login')):
        admin_get = requests.get('http://192.168.1.25:5000/get-admin-by-id/'+str(login_get)).json()
    else:
        return redirect('/dashboard/adminLogin/')
    form = User(request.GET, request.FILES)
    return render(request,'UserPostView.html',{'form':form,'admin_get':admin_get})




def handle_uploaded_file(f,abs_file_path):
    with open(abs_file_path, 'wb') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
    

def PostUser(request):
    context = {}
    ROOT_FOLDER = os.path.dirname(os.path.abspath(__file__))
    print(ROOT_FOLDER)
    target = os.path.join(ROOT_FOLDER,'images-user')
    print(target)
    if not os.path.isdir(target):
        os.mkdir(target)


    if request.method == 'POST':
        form = User(request.POST, request.FILES)
        if form.is_valid():
            image = request.FILES['image']
            image_name = image.name
            img = Image.open(image)
            abs_file_path = '/'.join([target,image.name])
            img.save(abs_file_path)
            img.close()
            firstname = form.cleaned_data['firstname']
            lastname = form.cleaned_data['lastname']
            password = form.cleaned_data['phone']
            email = form.cleaned_data['email']
            address = form.cleaned_data['address']
            phone = form.cleaned_data['password']

            data1={'firstname':firstname,'lastname':lastname, 'password':phone,'email':email,'address':address,'phone':password}
            print(data1)
            multipart_form_data = {
                'image': (image_name, open(abs_file_path, 'rb')),
                'firstname': (None, firstname),
                'lastname': (None, lastname),
                'password': (None, password),
                'email': (None, email),
                'address': (None, address),
                'phone': (None, phone),
            }
            r = requests.post('http://192.168.1.25:5000/post-user/',files=multipart_form_data )
            print(r.text)
       

        else:
            form = User() 
            print('post fail')
        
        return redirect('/dashboard/dashboarduser/')




from contextlib import redirect_stderr
from itertools import product
import json
from multiprocessing import context
from django.template import loader
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect
import requests,urllib3
from PIL import Image
import collections 
import os
from module.UserModule.form.forms_user import User_Edit
import sys
if sys.version_info.major == 3 and sys.version_info.minor >= 10:
    from collections.abc import MutableMapping
else:
    from collections import MutableMapping


def index(request,id):
    if request.method == 'GET':
        login_get = json.loads(json.dumps(request.session.get('login')))
        print('login_get')
        if(request.session.get('login')):
            admin_get = requests.get('http://192.168.1.25:5000/get-admin-by-id/'+str(login_get)).json()
        else:
            return redirect('/dashboard/adminLogin/')
        
        data = requests.get(f'http://192.168.1.25:5000/get-user-by-id/{id}',headers = {'Content-Type' : 'application/json'}).json()
        print(data)
        form=User_Edit(initial=data)
        my_dict = {'form' : form,'data_id' : data,'admin_get':admin_get }
        return render(request,'UserUpdateView.html',context = my_dict)

def update(request,id):
    context = {}
    ROOT_FOLDER = os.path.dirname(os.path.abspath(__file__))
    print(ROOT_FOLDER)
    target = os.path.join(ROOT_FOLDER,'images')
    print(target)
    if not os.path.isdir(target):
        os.mkdir(target)
    if request.method == 'POST':
        form = User_Edit(request.POST, request.FILES)
        if form.is_valid():
            image = request.FILES['image']
            image_name = image.name
            img = Image.open(image)
            abs_file_path = '/'.join([target,image.name])
            img.save(abs_file_path)
            img.close()
            firstname = form.cleaned_data['firstname']
            lastname = form.cleaned_data['lastname']
            password = form.cleaned_data['password']
            email = form.cleaned_data['email']
            address = form.cleaned_data['address']
            phone = form.cleaned_data['phone']
            abs_file_path_form = '/'.join([target, image_name])
            multipart_form_data = {
                'image': (image_name, open(abs_file_path_form, 'rb') ),
                'firstname': (None, firstname),
                'lastname': (None, lastname),
                'password': (None, password),
                'email': (None, email),
                'address': (None, address),
                'phone': (None, phone),
            }
            data = requests.post(f'http://192.168.1.25:5000/update-user/'+ str(id),files=multipart_form_data)
            try:
                data.save()
            except:
                print('save fail')
            context = {'user_data' :  data}
        else:
            form = User_Edit() 
        
        return redirect('/dashboard/dashboarduser/')
        
        







// function hide_show_checkbox() {
//   var x = document.getElementById("checkbox-tick-delete");
//   for (let i = 0; i < x.length; i++) {
//     if (x[i].style.display === "block") {
//       x[i].style.display = "none";
//       console.log("off");
//     } else {
//       x[i].style.display = "block";
//       console.log("on");
//     }
//   }
// }

function hide_show_checkbox() {
  const div = document.querySelectorAll("#checkbox-tick-delete");

  for (let i = 0; i < div.length; i++) {
    const styles = window.getComputedStyle(div[i]);

    if (styles.display == "block") {
      div[i].style.display = "none";
    } else {
      div[i].style.display = "block";
    }
  }
}

from atexit import register
from contextlib import redirect_stderr
from django import template
from django.template import loader
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect
import requests,urllib3
import collections 
import json
import os, base64
from django.contrib import messages
from django.core.paginator import Paginator
# from ..module.ProductModule import view
import sys
if sys.version_info.major == 3 and sys.version_info.minor >= 10:
    from collections.abc import MutableMapping
else:
    from collections import MutableMapping
    
def index(request):
    
        if request.method == 'GET':

            return render(request,'AdminLoginView.html')
        if request.method == 'POST':
            get_admin = requests.get('http://192.168.1.25:5000/get-admin/').json()
            admin_get_load = json.loads(json.dumps(get_admin))
            print(admin_get_load)
            username = request.POST['username']
            password = request.POST['password']
            login = False
            for admin in admin_get_load:
                if(admin['username'] == username and admin['password'] == password):
                    request.session['login']=admin['id']
                    login = True
                    print('login')   
                    break
                else:
                    login = False
                    print('dont login to page')   
            if login:
                messages.success(request, 'login success')
                return redirect('/dashboard/dashboard/')
            else:
                messages.warning(request, 'login fail')
                return redirect('/dashboard/adminLogin/')
            


def logout(request):
    if(request.session.get('login')):
        del request.session['login']
        return redirect('/dashboard/adminLogin/')
    else:
        return redirect('/dashboard/adminLogin/')
"""cms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
# from module.view import dashboard
from django.conf import settings
from django.conf.urls.static import static
app_name = "module"
urlpatterns = [
    path('dashboard/', include('module.urls')),
    path('dashboarduser/', include('module.urls')),
    path('dashboard/<int:id>', include('module.urls')),
    path('dashboarduser/<int:id>', include('module.urls')),
    path('postproduct/', include('module.urls')),
    path('postuser/', include('module.urls')),

    path('updateproduct/', include('module.urls')),
    path('updateuser/', include('module.urls')),

    
    path('admin/', admin.site.urls),
]+ static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
